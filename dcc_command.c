/* (c) 2018 crepi22 
 * Redistributed under the terms of the BSD 3 clause license 
 */

#include <stdio.h>
#include<stdlib.h>
#include <avr/io.h>
#include <util/delay.h>
#define BAUD 115600
#include <util/setbaud.h>
#include <avr/interrupt.h>
#include "uart.h"

// We assume a scaler of 8 and a 16Mhz clock (should be changed if 3.3v)
// length or a low or high level for a 1
const uint16_t ONE_PULSE = 115;   // 58 mu s
// length or a low or high level for a 0
const uint16_t ZERO_PULSE = 201;  // 101 mu s
// Pin used for DCC output (forced by timer 1)
const uint8_t DCC_PIN = 9;
// Pin used for track power control (arbitrary digital pin not 9)
const uint8_t PWR_PIN = 4;
// Length of command buffer
#define BUFFER_LEN 512
// Length of input line
#define LINE_LENGTH 128
// Lenght of auxiliary line 
#define AUX_LEN 32
// Length of debug output line 
#define DBG_LEN 128

// Number of reset to send for a correct reinit.
const uint8_t NB_RESET_AT_REINIT = 20;

enum dcc_state {
  IDLE0,
  IDLE1,
  PREAMBLE0,
  PREAMBLE1,
  BYTE0,
  BYTE1,
  SEP0,
  SEP1,
  END0,
  END1
};
// Buffer of next DCC commands to send
uint8_t buffer[BUFFER_LEN];
// pointer on next command to read in the command buffer.
uint16_t read_pos;
// pointer on next command to write in the command buffer.
uint16_t write_pos;
// 
uint16_t len;
// Number of reset commands to send to reinit decoders.
uint8_t reinit;

// ISR current state
enum dcc_state isr_state;

// the length of the current packet. 
// Also used as a sync lock between timer ISR and main program
volatile uint8_t pkt_length;
// Packet to output
uint8_t packet[6];
// bit count for ISR state
uint8_t count;
// byte count for isr state
uint8_t cur_byte;

// input line for main program
uint8_t line[LINE_LENGTH];
// Debug line.
char dbg_line[DBG_LEN];
// auxiliary output line
char aux_line[AUX_LEN];
// Debug line position
uint8_t dbg_k = 0;

uint8_t debug_level = 2;

void dcc_init() {
  uart_init();
  stdout = &uart_output;
  stdin  = &uart_input;

  // DCC signal output mode on PIN 9
  DDRB |= _BV(DDB1);
  // Track current control on PIN 4
  DDRD |= _BV(DDD4);
  // No current when we start
  PORTD &= ~_BV(PORTD4);
  // digitalWrite(PWR_PIN, LOW);

  // We need to setup timer 1 in CTC with TOP on OCR1A mode for PIN 9.
  TCCR1B = (1 << WGM12) | (1 << CS11);
  // We toggle output of OC1A (Pin 9) on compare match
  TCCR1A = (1 << COM1A0);
  // Initialize the timer.
  OCR1A = ZERO_PULSE;
  // Initial state idle for ISR automaton
  isr_state = IDLE0;
  // We activate interupt on compare match
  TIMSK1 |= (1 << OCIE1A);

  read_pos = write_pos = 0;
  reinit = NB_RESET_AT_REINIT;
}

ISR(TIMER1_COMPA_vect) {
  dbg_line[dbg_k++] = (OCR1A == ONE_PULSE) ? '1' : '0';
  switch (isr_state) {
    case IDLE0:
      isr_state = IDLE1;
      dbg_k--;
      break;
    case PREAMBLE0:
      isr_state = PREAMBLE1;
      dbg_k--;
      break;
    case SEP0:
      isr_state = SEP1;
      dbg_k--;
      break;
    case BYTE0:
      isr_state = BYTE1;
      dbg_k--;
      break;
    case END0:
      isr_state = END1;
      dbg_k--;
      break;
    case IDLE1:
      if (pkt_length) {
        OCR1A = ONE_PULSE;
        count = 14;
        isr_state = PREAMBLE0;
      } else {
        isr_state = IDLE0;
        dbg_k--;
      }
      break;
    case PREAMBLE1:
      if (--count) {
        isr_state = PREAMBLE0;
      } else {
        isr_state = SEP0;
        count = 8;
        cur_byte = 0;
        OCR1A = ZERO_PULSE;
      }
      break;
    case SEP1:
      count = 7;
      OCR1A = (packet[cur_byte] & (1 << count)) ? ONE_PULSE : ZERO_PULSE;
      isr_state = BYTE0;
      break;
    case BYTE1:
      if (count--) {
        OCR1A = (packet[cur_byte] & (1 << count)) ? ONE_PULSE : ZERO_PULSE;
        isr_state = BYTE0;
      } else {
        if (++cur_byte < pkt_length) {
          OCR1A = ZERO_PULSE;
          isr_state = SEP0;
        } else {
          OCR1A = ONE_PULSE;
          isr_state = END0;
        }
      }
      break;
    case END1:
      dbg_line[dbg_k++] = 0;
      pkt_length = 0;
      OCR1A = ZERO_PULSE;
      isr_state = IDLE0;
      break;
  }
}

void dcc_proceed() {
  uint8_t *cursor;
  while (is_available()) {
    int line_len = read_until('\r', line, LINE_LENGTH);
    if (line_len > 0) {
      line[line_len] = 0;
      switch (line[0]) {
        case '+':
          // Set D4 to 1
          PORTD |= _BV(PORTD4);
          reinit = NB_RESET_AT_REINIT;
          if (debug_level) {
            puts("*** Power ON ***\n");
          }
          break;
        case '-':
          // Set D4 to 0
          PORTD &= ~_BV(PORTD4);
          if (debug_level) {
            puts("*** Power OFF ***\n");
          }
          break;
        case 'D':
          cursor = line + 1;
          debug_level = (uint8_t)(strtol(cursor, &cursor, 10) & 0xff);
          puts("*** Debug level ");
          itoa(debug_level, aux_line, 10);
          puts(aux_line);
          puts(" ***\n");
          break;
        case 'O':
          cursor = line + 1;
          int len = 0;
          while (*cursor != '\r' && cursor < line + line_len) {
            len = len + 1;
            uint8_t next_byte = (uint8_t)(strtol(cursor, &cursor, 16) & 0xff);
            buffer[(write_pos + len) % BUFFER_LEN] = next_byte;
          }
          if (debug_level) {
            int i;
            puts("*** Command");
            for(i=0; i < len; i++) {
              puts(" ");
              itoa(buffer[write_pos+i], aux_line, 16);
              puts(aux_line);
            }
            puts(" ***\n");
          }
          buffer[write_pos] = len;
          write_pos = (write_pos + len + 1) % BUFFER_LEN;
      }
    }
  }
  if (pkt_length == 0) {
    if (dbg_k != 0) {
      if (debug_level > 2) {
        puts("  Sent: ");
        puts(dbg_line);
        putchar('\n');
      }
      dbg_k = 0;
    }
    if (reinit > 0) {
      reinit--;
      packet[0] = 0;
      packet[1] = 0;
      packet[2] = 0;
      pkt_length = 3;
    } else {
      if (read_pos != write_pos) {
        int len = buffer[read_pos];
        int i;
        for (i = 0; i < len; i++) {
          packet[i] = buffer[(read_pos + i + 1) % BUFFER_LEN];
        }
        if (debug_level > 1) {
          puts("  Next:");
          int i;
          for (i = 0; i < len; i++) {
            putchar(' ');
            itoa(packet[i], aux_line, 10);
            puts(aux_line);
          }
          putchar('\n');
        }
        pkt_length = len;
        read_pos = (read_pos + len + 1) % BUFFER_LEN;
      } else {
        packet[0] = 0xFF;
        packet[1] = 0;
        packet[2] = 0xFF;
        pkt_length = 3;
      }
    }
  }
}

int main(void)
{
  dcc_init();
  while(1) dcc_proceed();
}
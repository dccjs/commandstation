CC=avr-gcc
CFLAGS=-Os -DF_CPU=16000000UL -mmcu=atmega328p 
dccjs-command: uart.o dcc_command.o
	avr-gcc -mmcu=atmega328p uart.o dcc_command.o -o $@ 
	avr-objcopy -O ihex -R .eeprom $@ $@.hex 
	avrdude -F -V -c arduino -p ATMEGA328P -P /dev/ttyUSB0 -b 57600 -U flash:w:$@.hex

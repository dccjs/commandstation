/* (c) 2018 crepi22 
 * Redistributed under the terms of the BSD 3 clause license 
 */

#ifndef _UART_H
#define _UART_H

#include<stdio.h>

void uart_init();
int is_available();
int read_until(char c, char* buffer, int limit);
FILE uart_output;
FILE uart_input;

#endif /* uart.h */
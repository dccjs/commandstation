/* (c) 2018 crepi22 
 * Redistributed under the terms of the BSD 3 clause license 
 */

#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#define BAUD 38400
#include <util/setbaud.h>

#define BUFLEN (1 << 5)

struct buffer {
    volatile uint8_t r;
    volatile uint8_t w;
    uint8_t data[BUFLEN];
};

static inline void init_buffer(struct buffer *b) {
    b->w = 0;
    b->r = 0;
}

static inline uint8_t read_buffer(struct buffer *b) {
    uint8_t c = b->data[b->r];
    b->r = (b->r + 1) & (BUFLEN - 1);
    return c;
}

static inline void write_buffer(struct buffer *b, uint8_t v) {
    b->data[b->w] = v;
    b->w = (b->w + 1) & (BUFLEN - 1);
}

static inline int is_empty(struct buffer *b) {
    return (b->r == b->w);
}

static inline int not_full(struct buffer *b) {
    return ((b->r - b->w) & (BUFLEN - 1)) != 1;
}

struct buffer rx_buff;
struct buffer tx_buff;
uint8_t tx_sleep = 1;
uint8_t c = 0;

void uart_init(void) {
    UBRR0H = UBRRH_VALUE;
    UBRR0L = UBRRL_VALUE;

#if USE_2X
    UCSR0A |= _BV(U2X0);
#else
    UCSR0A &= ~(_BV(U2X0));
#endif

    UCSR0C = _BV(UCSZ01) | _BV(UCSZ00); /* 8-bit data */
    /* Enable RX and TX and interupts*/
    UCSR0B = _BV(RXEN0) | _BV(TXEN0) | _BV(RXCIE0) | _BV(UDRIE0);   
    init_buffer(&rx_buff);
    init_buffer(&tx_buff);
    sei();
}


ISR(USART_RX_vect)
{
    if (not_full(&rx_buff)) {
	    write_buffer(&rx_buff, UDR0);
    }
}


ISR(USART_UDRE_vect)
{
    if (is_empty(&tx_buff)) {
        // Last byte transmitted. Sleeping.       
        tx_sleep = 1;	    
    } else {
        UDR0 = read_buffer(&tx_buff);
    }
}

void uart_putchar(char c, FILE *stream) {
    if (c == '\n') {
        uart_putchar('\r', stream);
    }
    if (not_full(&tx_buff)) {
        if (tx_sleep) {
            // TX was asleep. Awaking it.
            UDR0 = c;
            tx_sleep=0;
        } else {
            // TX working. Add to buffer
            write_buffer(&tx_buff, c);
        }
    }
}

char uart_getchar(FILE *stream) {
    while (is_empty(&rx_buff)) {}
    return read_buffer(&rx_buff);
}

int is_available() {
    return !is_empty(&rx_buff);
}

int read_until(char expected, char *buffer, int limit) {
    char c;
    limit--;
    int k = 0;
    while(k < limit) {
        while (is_empty(&rx_buff)) {}
        c = read_buffer(&rx_buff);
        if (c == expected) break;
        buffer[k] = c;
        k++;
    }
    buffer[k] = 0;
    return k;
}

FILE uart_output = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);
FILE uart_input = FDEV_SETUP_STREAM(NULL, uart_getchar, _FDEV_SETUP_READ);
 
# DCCJS-command
A minimalist DCC command station for DCCJS written in pure C. 

The code will execute on an Arduino (tested with an Arduino Nano) wired to a LMD18200 module.

* PIN9 of the Arduino must be wired to DIR.
* PIN4 can be used to control if power is available on the LMD18200.

The command station uses a protocol similar to a subset of SPROG(TM):

<dl>
<dt>+</dt>
<dd>Turn on power</dd>
<dt>-</dt>
<dd>Turn off power </dd>
<dt>O h1 ... hn</dt>
<dd></dd>
<dt>D n</dt>
<dd>Set the debugging level
  <ul>
  <li> 0 no info
  <li> 1 basic command
  <li> 2 command sent to the track (hex form)
  <li> 3 bits sent to the track (binary raw format)
  </ul>
</dd>

## Installation 

You must have `avr-gcc`, `avr-libc` and `avr-dude` on your system. The Makefile has only been tested on a Linux system. Just type `make`
